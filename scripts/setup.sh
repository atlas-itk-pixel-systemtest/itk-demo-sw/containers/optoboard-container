_pythonpath=$(dirname $(find /opt/lcg -name python | grep bin))
export PATH=${_pythonpath}:$PATH

for i in $(find /opt/lcg/ -name site-packages -type d)
do
  PYTHONPATH=${i}:${PYTHONPATH}
done
PYTHONPATH=/installed/ic_comm/:${PYTHONPATH}
# export PATH=/installed/felix-master-rm5-stand-alone/x86_64-centos7-gcc11-opt/bin:${PATH}
unset _pythonpath
export PYTHONPATH

