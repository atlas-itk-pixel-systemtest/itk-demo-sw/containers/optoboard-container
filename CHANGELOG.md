# Changelog

All notable changes[^1] to the `optoboard-container` repo.

This repo is "living at head" which means tags correspond to the overall repo.
All subrepos/workspaces, all packages in the package registry and all container images in the image registry follow the repo tag.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

(always keep this section to take note of untagged changes at HEAD)

## [1.1.1] - 2023-07-21

### Changed

- tdaq-less build of ic-over-netio.
- use almalinux:9 based build by default.

## [1.0.0] - 2023-xx-xx

[1.1.1]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/optoboard-container/-/compare/1.0.0...1.1.1
[1.0.0]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/optoboard-container/-/tags/1.0.0
