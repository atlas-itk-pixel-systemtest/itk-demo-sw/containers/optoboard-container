# Optoboard-container + Felix API example

## Installation

for the Optoboard stack no installation is needed apart from Docker

(check the values set to the .env file are correct)

start the stack

```shell
docker compose up
```

Initialize the Optoboard shim
```shell
./setup
```
now it is possible to run the following command inside the container from any path of the host machine
```
InitOpto -i -optoboard_serial 33000000 -vtrx_v 1.3 -flx_G 0 -flx_d 0
```