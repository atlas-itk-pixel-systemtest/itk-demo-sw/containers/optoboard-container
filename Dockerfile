FROM gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/base-05-00-03-rm5-el9:demi-2

WORKDIR /root

# RUN dnf -y install git cmake python3-devel dnf-plugins-core gcc gcc-c++


# Add artifacts
COPY ic-over-netio-build /root/ic-over-netio-build
# COPY ic-over-netio-next-build /root/ic-over-netio-next-build
COPY lpgbt-com-build /root/lpgbt-com-build


RUN dnf -y install pip git && \
    PYTHON_VERSION_SHORT=$(python3 --version 2>&1 | awk '{print $2}' | cut -d '.' -f 1,2 | tr -d '.') && \
    machine_architecture=$(uname -m) && \
    cp /root/lpgbt-com-build/lpgbtcom.cpython-${PYTHON_VERSION_SHORT}-${machine_architecture}-linux-gnu.so /usr/lib64/python3.9/site-packages/ && \
    cp /root/lpgbt-com-build/liblpgbt-com.so /felix-05-00-03-rm5-stand-alone/x86_64-el9-gcc13-opt/lib && \
    # cp /root/lpgbt-com-build/x86_64-el9-gcc13-opt/lib/lpgbtcom.so /usr/lib64/python3.9/site-packages/ && \
    # cp /root/lpgbt-com-build/x86_64-el9-gcc13-opt/lib/liblpgbt-com.so /felix-05-00-03-rm5-stand-alone/x86_64-el9-gcc13-opt/lib && \
    # cp /root/ic-over-netio-next-build/libitk_ic_over_netio_next.so /usr/lib64/python3.9/site-packages/ && \
    cp /root/ic-over-netio-build/libic_comms.cpython-${PYTHON_VERSION_SHORT}-${machine_architecture}-linux-gnu.so /usr/lib64/python3.9/site-packages/

### Add s6-overlay

# set version for s6 overlay
ARG S6_OVERLAY_VERSION="3.1.5.0"
ARG S6_OVERLAY_ARCH="x86_64"

# add s6 overlay
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-${S6_OVERLAY_ARCH}.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-${S6_OVERLAY_ARCH}.tar.xz && \
    tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz

# environment variables

ENV PS1="[Docker] $(whoami)@$(hostname):$(pwd)\\$ " \
    HOME="/root" \
    TERM="xterm" \
    S6_CMD_WAIT_FOR_SERVICES_MAXTIME="0" \
    S6_VERBOSITY=1 \
    S6_KEEP_ENV=1 \
    LD_LIBRARY_PATH=/felix-05-00-03-rm5-stand-alone/x86_64-el9-gcc13-opt/lib:$LD_LIBRARY_PATH
#   S6_STAGE2_HOOK=/docker-mods \
#   VIRTUAL_ENV=/lsiopy \
#   PATH="/lsiopy/bin:$PATH"

# Install optoboard-felix and service-registry
RUN python3 -m pip install optoboard-felix --index-url https://gitlab.cern.ch/api/v4/projects/113584/packages/pypi/simple  && \
    python3 -m pip install service-registry --index-url https://gitlab.cern.ch/api/v4/projects/145742/packages/pypi/simple  && \
    python3 -m pip install config-checker --index-url https://gitlab.cern.ch/api/v4/projects/163911/packages/pypi/simple && \
    echo pip show optoboard-felix
    

### Add itk user
### Not needed if we use the felix image as base image
# RUN useradd -u 911 -U -d /config -s /bin/bash itk && \
#     usermod -G users itk && \
RUN mkdir -p /config /results

# add local files
COPY root/ /

ENTRYPOINT ["/init"]

# old entrypoint without s6
# ENV INITOPTO_ARGS=${INITOPTO_ARGS:-/bin/bash}
# ENTRYPOINT ["/bin/bash", "--login", "-c", "${INITOPTO_ARGS}"]


# --------------------

# COPY scripts /scripts
# WORKDIR /root

#RUN python3 -m ensurepip && \
#    python3 -m pip install --no-cache-dir -U pip

#RUN source ./bootstrap.sh && \
#    source ./setup.sh && \

# Add binaries for lpgbt-com
# RUN git clone https://gitlab.cern.ch/itk-daq-sw/build.git lpgbt-com
# RUN echo '. lpgbt-com/setup.sh' >>~/.bashrc

# RUN . /scripts/setup.sh && \
# RUN python3 -m ensurepip && \
#    python3 -m pip install --no-cache-dir -U pip && \
# copy standard configs
# needs optoboard_felix checked out locally
# COPY optoboard_felix/configs/ /config

# COPY patch/CommConfig.py /opt/lcg/Python/3.9.6-b0f98/x86_64-centos7-gcc11-opt/lib/python3.9/site-packages/optoboard_felix/driver/CommConfig.py

# RUN /scripts/config.sh

# RUN echo '. /scripts/setup.sh' >>~/.bashrc

### Add Docker

# ADD https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/devcontainers/-/raw/master/src/base-cc7/library-scripts/docker.sh .
# RUN sh docker.sh
