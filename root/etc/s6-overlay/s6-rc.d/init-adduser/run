#!/command/with-contenv bash
# shellcheck shell=bash

[ -v PUID ] || printf "**** WARNING PUID not set this will most likely lead to permissions problems! ****\n"
[ -v PGID ] || printf "**** WARNING PGID not set this will most likely lead to permissions problems! ****\n"

PUID=${PUID:-1000}
PGID=${PGID:-1000}

[ "$(id -g itk)" -ne "$PGID" ] && groupmod -o -g "$PGID" itk
[ "$(id -u itk)" -ne "$PUID" ] && usermod -o -u "$PUID" itk

# cat /etc/s6-overlay/s6-rc.d/init-adduser/branding

echo "
───────────────────────────────────────
Welcome to DeMi optoboard-container
https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/optoboard-container

───────────────────────────────────────
User UID:    $(id -u itk)
User GID:    $(id -g itk)
───────────────────────────────────────
"

# Noisy chown alias to handle read-only/remote volumes
cat <<-EOF >/usr/bin/demiown
#!/bin/bash
chown "\$@" || printf '**** Permissions to $2 could not be set. This is probably because your volume mounts are remote or read-only. ****\n**** The app may not work properly. ****\n'
EOF
chmod +x /usr/bin/demiown

cat <<-EOF >/usr/bin/demimod
#!/bin/bash
chmod "\$@" || printf '**** Permissions to $2 could not be set. This is probably because your volume mounts are remote or read-only. ****\n**** The app may not work properly. ****\n'
EOF
chmod +x /usr/bin/demimod

demiown itk:itk /config
demiown itk:itk /results
demiown -R itk:itk /root

demimod 775 /config
demimod 775 /results
demimod -R 775 /root
